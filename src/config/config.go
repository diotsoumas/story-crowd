package config

import (
	"encoding/json"
	"os"
	"log"
)

//Config structure that holds all application specific config configuration
type Config struct {
	Options map[string]string
}

func (c Config) GetOptions()Config {
	file, _ := os.Open("config/config.json")
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&c.Options)
	if err != nil {
	  log.Println("error:", err)
	}

	return c
}
