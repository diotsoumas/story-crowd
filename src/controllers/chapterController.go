package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"models"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
)

// ChapterController structure
type (
	ChapterController struct {
		connection *sql.DB
	}
)

/*
NewChapterController Instantiates a new ChapterController instance.
*/
func NewChapterController(s *sql.DB) *ChapterController {
	return &ChapterController{s}
}

/*
GetChapter returns a specific story
*/
func (uc ChapterController) GetChapter(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	s := new(models.Chapter)
	res, err := uc.connection.Query("SELECT * from chapters WHERE id = ?", p.ByName("id"))
	checkErr(err)
	defer res.Close()

	for res.Next() {
		err := res.Scan(&s.ID, &s.CreatedByUser, &s.StoryID, &s.Text, &s.CreatedDate, &s.Title)
		checkErr(err)
	}
	// Marshal provided interface into JSON structure
	sj, _ := json.Marshal(s)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprintf(w, "%s", sj)
}

/*
CreateChapter creates a new chapter entity
*/
func (uc ChapterController) CreateChapter(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	//Parse parameters from request
	r.ParseForm()
	chapterRepository := new(models.Chapter)
	c := chapterRepository.GetChapterFromPost(r.Form)

	// insert
	stmt, err := uc.connection.Prepare("INSERT INTO chapters(title, createdByUser, storyId, text, createdDate) values(?,?,?,?,?)")
	checkErr(err)

	_, err = stmt.Exec(c.Title, c.CreatedByUser, c.StoryID, c.Text, time.Now())
	checkErr(err)
	// Marshal provided interface into JSON structure
	cj, _ := json.Marshal(c)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", cj)
}
