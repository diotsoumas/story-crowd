package models

import (
	"net/url"
	"strconv"
	"time"
)

//Chapter structure
type Chapter struct {
	ID            string    `json:"id"`
	Title         string    `json:"title"`
	CreatedByUser int       `json:"createdByUser"`
	CreatedDate   time.Time `json:"createdDate"`
	Text          string    `json:"text"`
	StoryID       int       `json:"storyId"`
}

/*
GetChapterFromPost returns a chapter object when given some POST data
*/
func (c Chapter) GetChapterFromPost(values url.Values) Chapter {
	c.Title = values.Get("Title")
	userID, _ := strconv.Atoi(values.Get("CreatedByUser"))
	c.CreatedByUser = userID
	storyID, _ := strconv.Atoi(values.Get("StoryID"))
	c.StoryID = storyID
	//TODO:How can I test 'text' for injection in go?
	c.Text = values.Get("Text")
	return c
}
