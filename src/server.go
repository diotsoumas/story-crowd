package main

import (
	// Standard library packages
	"config"
	"controllers"
	"database/sql"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	// Instantiate a new router
	r := httprouter.New()
	// Instantiate the database
	db, err := sql.Open("sqlite3", "./story-crowd.db")
	checkErr(err)
	//Do I need to close the db session afterwards?
	//defer db.Close()

	//Load Configuration
	c := new(config.Config)
	configuration := c.GetOptions()
	log.Println("Loaded configuration ", configuration)

	//routes for the user controllers
	userController := controllers.NewUserController(db)
	r.GET("/user/:id", userController.GetUser)
	r.POST("/user", userController.CreateUser)

	//routes for the story controllers
	storyController := controllers.NewStoryController(db)
	r.GET("/story/:id", storyController.GetStory)
	r.POST("/story", storyController.CreateStory)

	//routes for the chapter controllers
	chapterController := controllers.NewChapterController(db)
	r.GET("/chapter/:id", chapterController.GetChapter)
	r.POST("/chapter", chapterController.CreateChapter)

	// Fire up the server
	http.ListenAndServe("localhost:3000", r)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
