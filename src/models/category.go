package models

//Category structure
type Category struct {
	Title       string
	Description string
}

func (c Category) getCategories() map[string]string {
	categories := make(map[string]string)
	categories["Funny"] = "A funny category"
	categories["Mystery"] = "A category about mysteries"
	return categories
}
