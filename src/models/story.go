package models

import (
	"net/url"
	"strconv"
	"time"
)

//Story structure
type Story struct {
	ID                  string    `json:"id"`
	Title               string    `json:"title"`
	CreatedByUser       int       `json:"createdByUser"`
	Category            string    `json:"createdDate"` // Category structure
	CreatedDate         time.Time `json:"createdDate"`
	ChaptersUntilFinale int       `json:"chaptersUntilFinale"`
}

/*
GetStoryFromPost returns a user object when given some POST data
*/
func (s Story) GetStoryFromPost(values url.Values) Story {
	s.Title = values.Get("Title")
	userID, _ := strconv.Atoi(values.Get("CreatedByUser"))
	s.CreatedByUser = userID
	s.Category = values.Get("Category")
	i, _ := strconv.Atoi(values.Get("ChaptersUntilFinale"))
	s.ChaptersUntilFinale = i
	return s
}
