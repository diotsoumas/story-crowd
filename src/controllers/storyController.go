package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"models"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
)

// StoryController structure
type (
	StoryController struct {
		connection *sql.DB
	}
)

/*
NewStoryController Instantiates a new StoryController instance.
*/
func NewStoryController(s *sql.DB) *StoryController {
	return &StoryController{s}
}

/*
GetStory returns a specific story
*/
func (uc StoryController) GetStory(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	s := new(models.Story)
	res, err := uc.connection.Query("SELECT * from stories WHERE id = ?", p.ByName("id"))
	checkErr(err)
	defer res.Close()

	for res.Next() {
		err := res.Scan(&s.ID, &s.Title, &s.CreatedByUser, &s.Category, &s.CreatedDate, &s.ChaptersUntilFinale)
		checkErr(err)
	}
	// Marshal provided interface into JSON structure
	sj, _ := json.Marshal(s)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprintf(w, "%s", sj)
}

/*
CreateStory creates a new story entity
*/
func (uc StoryController) CreateStory(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	//Parse parameters from request
	r.ParseForm()
	storyRepository := new(models.Story)
	s := storyRepository.GetStoryFromPost(r.Form)

	// insert
	stmt, err := uc.connection.Prepare("INSERT INTO stories(title, createdByUser, category, createdDate, chaptersUntilFinale) values(?,?,?,?,?)")
	checkErr(err)

	_, err = stmt.Exec(s.Title, s.CreatedByUser, s.Category, time.Now(), s.ChaptersUntilFinale)
	checkErr(err)

	// Marshal provided interface into JSON structure
	uj, _ := json.Marshal(s)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", uj)
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
