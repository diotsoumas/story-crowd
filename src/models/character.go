package models

//Character structure
type Character struct {
	ID          string
	StoryID     int
	Name        string
	Surname     string
	Occupation  string
	Description string
	Status      string
}
