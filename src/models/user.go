package models

import (
	"net/url"
	"strconv"
	"time"
)

//User structure
type User struct {
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	Surname     string    `json:"surname"`
	Age         int       `json:"age"`
	Facebook    string    `json:"facebook"`
	Email       string    `json:"email"`
	CreatedDate time.Time `json:"createdDate"`
}

/*
GetUserFromPost returns a user object when given some POST data
*/
func (u User) GetUserFromPost(values url.Values) User {
	u.Name = values.Get("Name")
	u.Surname = values.Get("Surname")
	//Update strin to integer
	i, _ := strconv.Atoi(values.Get("Age"))
	u.Age = i
	u.Facebook = values.Get("Facebook")
	u.Email = values.Get("Email")
	return u
}
