package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"models"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
)

// UserController structure
type (
	UserController struct {
		connection *sql.DB
	}
)

/*
NewUserController Instantiates a new USerController instance.
*/
func NewUserController(s *sql.DB) *UserController {
	return &UserController{s}
}

/*
GetUser returns a specific user
*/
func (uc UserController) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	u := new(models.User)
	res, err := uc.connection.Query("SELECT * from users WHERE id = ?", p.ByName("id"))
	checkErr(err)
	defer res.Close()

	for res.Next() {
		err := res.Scan(&u.ID, &u.Name, &u.Surname, &u.Age, &u.Email, &u.Facebook, &u.CreatedDate)
		checkErr(err)
	}
	// Marshal provided interface into JSON structure
	uj, _ := json.Marshal(u)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprintf(w, "%s", uj)
}

/*
CreateUser creates a new user entity
*/
func (uc UserController) CreateUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	//Parse parameters from request
	r.ParseForm()
	userRepository := new(models.User)
	userEntity := userRepository.GetUserFromPost(r.Form)

	// insert
	stmt, err := uc.connection.Prepare("INSERT INTO users(name, surname, age, facebook, email, createdDate) values(?,?,?,?,?,?)")
	checkErr(err)

	_, err = stmt.Exec(userEntity.Name, userEntity.Surname, userEntity.Age, userEntity.Facebook, userEntity.Email, time.Now())
	checkErr(err)

	// Marshal provided interface into JSON structure
	uj, _ := json.Marshal(userEntity)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", uj)
}
